import Foundation


// MARK:- Extention of UIColor

extension UIColor
{
    static func  colorFrom(red red : CGFloat, green : CGFloat, blue : CGFloat, alpha : CGFloat) -> UIColor
    {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
}

//Get green color

extension UIColor{
    static func greenThemeColor() -> UIColor{
        return UIColor(red: 153/255.0, green: 217/255.0, blue: 81/255.0, alpha: 1.0)
    }
    
    static func blurThemeColor() -> UIColor{
        return UIColor(red: 62/255.0, green: 107/255.0, blue: 183/255.0, alpha: 1.0)
    }
    
    static func greyThemeColor() -> UIColor{
        return UIColor(red: 239/255.0, green: 239/255.0, blue: 242/255.0, alpha: 1.0)
    }
    
    static func blueThemeColor() -> UIColor{
        return UIColor(red: 22/255.0, green: 122/255.0, blue: 255/255.0, alpha: 1.0)
    }
}

//Getting UIColor from Hex Value
extension UIColor
{
    static func colorWithHexString (hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercaseString
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substringFromIndex(1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.grayColor()
        }
        
        let rString = (cString as NSString).substringToIndex(2)
        let gString = ((cString as NSString).substringFromIndex(2) as NSString).substringToIndex(2)
        let bString = ((cString as NSString).substringFromIndex(4) as NSString).substringToIndex(2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        NSScanner(string: rString).scanHexInt(&r)
        NSScanner(string: gString).scanHexInt(&g)
        NSScanner(string: bString).scanHexInt(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
}

// Font Extension
extension UIFont {
    func systemFontOfSize(size: CGFloat) -> UIFont {
        return UIFont(name: "Roboto-Regular", size: size)!
    }
}