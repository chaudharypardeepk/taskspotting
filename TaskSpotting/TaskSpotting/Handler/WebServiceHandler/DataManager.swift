

import Foundation
import Alamofire
import MBProgressHUD

let KEY_DATA_HANDLER = "webHandlerQueue"

class DataManager: NSObject {
    
    static let shareInstance = DataManager()
    
    //Common Methods to call post and get services
    func getDataFromWebService(url:String,dataDictionary:NSDictionary?,requestType:String,isProtected:Bool,completionHandler:(result: AnyObject!,error:NSError!) -> Void)
    {
        print(dataDictionary)
        let urlValue: NSURL = NSURL(string: url)!
        
        let queue = dispatch_queue_create(KEY_DATA_HANDLER, DISPATCH_QUEUE_CONCURRENT)
        
        self.showProgressWithText(loadingKey)

        
        if(requestType == request_Type_Post)
        {
        if (isProtected == true)
        {
        let headerValues : Dictionary<String,String> = [:]
        
//        if let token : String = NSUserDefaults.standardUserDefaults().objectForKey(tokenKey) as? String {
//                headerValues[authorizationKey] = tokenKey + spaceStringKey + token
//        }
            
        let request = Alamofire.request(.POST, urlValue, parameters: dataDictionary as? [String : AnyObject],encoding: .JSON,headers: headerValues)
            request.response(
                queue: queue,
                responseSerializer: Request.JSONResponseSerializer(options: .AllowFragments),
                completionHandler: { response in
                    if(response.result.error == nil)
                    {
                        print(response.result.value)
                        self.removeProgress()
                        completionHandler(result: response.result.value!, error: nil)
                    }
                    else
                    {
                        print(response.result.error)
                        self.removeProgress()
                        completionHandler(result: nil, error: response.result.error )
                    }
                }
            )
        }
        else
        {
            let request = Alamofire.request(.POST, urlValue, parameters: dataDictionary as? [String : AnyObject])
            request.response(
                queue: queue,
                responseSerializer: Request.JSONResponseSerializer(options: .AllowFragments),
                completionHandler: { response in
                    if(response.result.error == nil)
                    {
                        print(response.result.value)
                        self.removeProgress()
                        completionHandler(result: response.result.value!, error: nil)
                    }
                    else
                    {
                        print(response.result.error)
                        self.removeProgress()
                        completionHandler(result: nil, error: response.result.error )
                    }
                }
            )
            
        }
        }
        else
        {
            if (isProtected == true)
            {
                var headerValues : Dictionary<String,String> = [:]
                        if let token : String = NSUserDefaults.standardUserDefaults().objectForKey(tokenKey) as? String {
                                headerValues[authorizationKey] = tokenKey + spaceStringKey + token
                        }
                
//                let token : String = NSUserDefaults.standardUserDefaults().objectForKey(tokenKey) as! String
//                headerValues[authorizationKey] = tokenKey + spaceStringKey + token
                
                let request = Alamofire.request(.GET, urlValue, parameters: dataDictionary as? [String : AnyObject],headers: headerValues)
                request.response(
                    queue: queue,
                    responseSerializer: Request.JSONResponseSerializer(options: .AllowFragments),
                    completionHandler: { response in
                        if(response.result.error == nil)
                        {
                            print(response.result.value)
                            self.removeProgress()
                            completionHandler(result: response.result.value!, error: nil)
                        }
                        else
                        {
                            print(response.result.error)
                            self.removeProgress()
                            completionHandler(result: nil, error: response.result.error )
                        }
                    }
                )
            }
            else
            {
                let request = Alamofire.request(.GET, urlValue, parameters: dataDictionary as? [String : AnyObject])
                request.response(
                    queue: queue,
                    responseSerializer: Request.JSONResponseSerializer(options: .AllowFragments),
                    completionHandler: { response in
                        if(response.result.error == nil)
                        {
                            print(response.result.value)
                            self.removeProgress()
                            completionHandler(result: response.result.value!, error: nil)
                        }
                        else
                        {
                            print(response.result.error)
                            self.removeProgress()
                            completionHandler(result: nil, error: response.result.error )
                        }
                    }
                )
                
            }
        }
    }
    
    //Common Methods for showing alert

    func showAlert(vc:UIViewController,message:String) -> Void
    {
        dispatch_async(dispatch_get_main_queue()) {
        let alert = UIAlertController(title: tatollKey, message:message, preferredStyle: .Alert)
        let action = UIAlertAction(title:oKey, style: .Default) { _ in
           vc.dismissViewControllerAnimated(true, completion: nil)
        }
        alert.addAction(action)
        vc.presentViewController(alert, animated: true){}
        }
    }
    
    //Common Methods for showing progress view

    func showProgressWithText(message: String?) -> Void
    {
        let loadingNotification = MBProgressHUD.showHUDAddedTo(appDelegate().window, animated: true)
        loadingNotification.mode = MBProgressHUDMode.Indeterminate
        loadingNotification.labelText = message
    }
    
    //Common Methods for removing progress view

    func removeProgress(){
        dispatch_async(dispatch_get_main_queue()) {
        MBProgressHUD.hideAllHUDsForView(appDelegate().window, animated: true)
        }
    }
    
}
