//
//  UtilityClass.swift
//  Tatoll
//
//  Created by Pardeep on 08/04/16.
//  Copyright © 2016 Rahul. All rights reserved.
//

import Foundation

class UtilityClass: NSObject
{
    class var sharedInstance: UtilityClass {
        struct Static {
            static let instance: UtilityClass = UtilityClass()
        }
        return Static.instance
    }
    
    /*
    class func saveUserDetails(userDetailObject : UserDetail)
    {
        let datos = NSKeyedArchiver.archivedDataWithRootObject(userDetailObject)
        NSUserDefaults.standardUserDefaults().setObject(datos, forKey: "userDetail")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func getUserDetail() -> UserDetail?
    {
        let decoded  = NSUserDefaults.standardUserDefaults().objectForKey("userDetail") as? NSData
        var decodedObject : UserDetail?
        if(decoded != nil)
        {
            decodedObject = NSKeyedUnarchiver.unarchiveObjectWithData(decoded!) as? UserDetail
            return decodedObject!
        }
        return decodedObject
        
    }*/
    
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluateWithObject(testStr)
        return result
    }
    
//    func showTipView()
//    {
//        
//    }
}