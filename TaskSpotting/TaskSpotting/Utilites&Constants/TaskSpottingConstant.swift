
import Foundation
import UIKit


let kScreenWidth = UIScreen.mainScreen().bounds.width
let kScreenHeight = UIScreen.mainScreen().bounds.height
var APP_DELEGATE : AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

// MARK: - API End Point
let serverURL = "http://reux.incao.co/api/"  //For Live


//Login Flow
//Twitter
let consumerKeyForTwitter = "RFUslRyEUCEs23w4S3QayIZWf"
let secretKeyForTwitter  = "JRXv1pCSR0xlBHh8iOLNek3fB6Q8ooK4s1seompPBlMNOkCdSb"

//Linkedin
let redirectUrlForLikedin = "https://beam.laserlike.com/social"
let stateForLikedin = "DLKDJF45DIWOERCM"
let clientIdForLikedin = "75j5e5qu4q1fhw"
let clientSecretForLikedin = "yf1c1202bjgqqWjA"

//Google
let clientIDForGoogle = "928216891274-p5mqe7kujgimfb92ep19aqv26bqtvhvp.apps.googleusercontent.com"



let mobileVerificationURL  = serverURL + "user/register.json"
let verificationCodeURL  = serverURL + "verify.json"
let updateProfileURL  = serverURL + "update/"
let deleteURL  = serverURL + "delete"
let reVerificationURL  = serverURL + "reverify.json"
let verifyEmail  = serverURL + "send-verify-mail.json"
let verifyEmailUsingCode  = serverURL + "verify-email.json"

let loginURL  = serverURL + "login/"

let categoryHomePage = serverURL + "taxonomy_term.json?fields=tid,name&parameters[vid]=2"
let categoryUsers = serverURL + "users-list/"
let addFavouriteProfile = serverURL + "add-favorite.json"
let userProfile = serverURL + "user/"

// MARK: - Social Login Keys

// MARK: - Keys

let auth_token = "auth_token"
let id = "id"
let social_flag = "social_flag"
let client = "client"
let device_id = "device_id"
let device_type = "2"
let email_key = "email"
let password_key = "password"
let request_Type_Post = "POST"
let request_Type_Get = "GET"
let tokenKey = "Token"
let authorizationKey = "Authorization"
let loadingKey  = "Loading"
let oKey = "OK"
let editKey = "Edit"
let spaceStringKey = " "
let deviceTokenString = "deviceToken"
let pushFlagKey = "push_flag"
let tatollKey = "Tatoll"
let messageKey = "msg"
let statusKey = "status"



//MARK: - Messages

let networkMessage = "Please check your internet connection."
let RESTRICT_FETCH_LOCATION = "Unable to fetch the location. Please Enable Your GPS."
let EnableLocation = "To enable, please go to Settings and turn on Location Service, for this app."
let UNABLE_FETCH_LOCATION = "Unable to fetch the location. Please Enable Your GPS"

let blankFieldMessage = "Please fill all the required fields."
let parsingFailMessage = "Unable to parse dictionary."
let accountNotFound = "Unable to find account."
let pushNotificationErrorMessge = "Please enable your push notification services from app settings."
let emptyMobileMessge = "Please enter mobile number."
let emptyTatoll = "Please enter tatoll number."
let emptyCode = "Please enter access code."
let IndustryInfoText = "IndustryInfoText"
let CompanyInfoText = "CompanyInfoText"
let DesignationInfoText = "DesignationInfoText"
let LanguageInfoText = "LanguageInfoText"
let TagInfoText = "TagInfoText"
let SocialMediaInfoText = "SocialMediaInfoText"
let EmailInfoText = "EmailInfoText"
let PanInfoText = "PanInfoInfoText"
let TotalVerificationInfoText = "TotalVerificationInfoText"



//MARK: - Status

let sucessStatus = 200

//MARK: - Three Bar Menu Keys

let titleForSetUp = "TATOLL SETUP"
let titleForMakeProfile = "MY PROFILE"
let titleForView = ""
let titleForMenu = ""
let cellIdentifier = ""
let headerViewHeight : CGFloat = 55.0
let buttonCornerRadius : CGFloat = 5.0

//color constants
let leftMenuBackColor =  UIColor.colorFrom(red: 110, green: 113, blue: 115, alpha: 1)
let leftMenuNavColor  =  UIColor.colorFrom(red: 161, green: 164, blue: 166, alpha: 1)
let leftMenuTextColor = UIColor.colorFrom(red: 55, green: 70, blue: 77, alpha: 1)
let leftMenuHeaderColor = UIColor.colorFrom(red: 203, green: 206, blue: 209, alpha: 1)

//Switch Constant
let one = 1
let two = 2
let three = 3
let four = 4
let five = 5


public enum HomeToolbarButton : Int {
    case Call           = 1
    case Message        = 2
    case Video          = 3
    case Dashboard      = 4
    case Wallet         = 5
}





